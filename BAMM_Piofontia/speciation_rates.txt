> mean(allrates$lambda)
[1] 0.9105097
> quantile(allrates$lambda, c(0.05, 0.95))
       5%       95% 
0.6087157 1.3670334 
> 
> mean(allrates$mu)
[1] 0.4249347
> quantile(allrates$mu, c(0.05, 0.95))
       5%       95% 
0.0360241 1.0553000 
> 
> #finally calculate speciation rates using Magallón and Sanderson's formula
> library(geiger)
> bd.ms(time=6.46, n=63, phy=NULL, epsilon = 0, missing = 0, crown=TRUE)
[1] 0.5340538
