#install.packages("phytools")
#install.packages("maps")
#install.packages("ape")
#install.packages()

setwd("/Users/oscar/Dropbox/Chapter_3/Biogeo_Pio")
dir()

library(ape)
library(maps)
library(phytools)
library(phangorn)

MaxCredNOLadUL <- read.tree("chrono_nr90hv5_2018_no.tree")

plot(MaxCredNOLadUL, cex=0.5, show.node.label=T)
axisPhylo()

#print all taxa
sort(MaxCredNOLadUL$tip.label)

#next command remove the outgroups
Pio_tree <- drop.tip(MaxCredNOLadUL, c("Archibaccharis_asperifolia","Aztecaster_matudae","Baccharis_genistelloides","Baccharis_tricuneata","Blakiella_bartsiifolia","Diplostephium_azureum","Diplostephium_barclayanum","Diplostephium_cajamarquillense","Diplostephium_callilepis","Diplostephium_cinereum","Diplostephium_crypteriophyllum","Diplostephium_empetrifolium","Diplostephium_ericoides","Diplostephium_espinosae","Diplostephium_foliosissimum","Diplostephium_glandulosum","Diplostephium_gnidioides","Diplostephium_goodspeedii","Diplostephium_gynoxyoides","Diplostephium_haenkei","Diplostephium_hartwegii","Diplostephium_hippophae","Diplostephium_jelskii","Diplostephium_juniperinum","Diplostephium_lechleri","Diplostephium_meyenii","Diplostephium_oblanceolatum","Diplostephium_oxapampanum","Diplostephium_pulchrum_OXA","Diplostephium_pulchrum_PAS","Diplostephium_sagasteguii","Diplostephium_serratifolium","Diplostephium_sp_nov_CAJ","Diplostephium_sp_nov_CAJ2","Diplostephium_sp_nov_JUN","Diplostephium_sp_nov_JUN2","Diplostephium_sp_nov_JUN3","Diplostephium_sp_nov_JUN4","Diplostephium_sp_nov_OXA","Diplostephium_spinulosum","Exostigma_notobellidiastrum","Floscaldasia_hypsophila","Heterothalamus_alienus","Hinterhubera_ericoides","Laennecia_sophiifolia","Laestadia_muscicola","Lagenophora_cuchumatanica","Parastrephia_quadrangularis","Westoniella_kohkemperi"))

plot(Pio_tree, cex=0.5, show.node.label=T)
axisPhylo()

is.ultrametric(Pio_tree)

write.tree(Pio_tree, file="pio_chrono_nr90hv5_2018_no.tree")