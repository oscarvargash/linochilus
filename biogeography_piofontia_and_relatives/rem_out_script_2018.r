#install.packages("phytools")
#install.packages("maps")
#install.packages("ape")
#install.packages()

setwd("/Users/oscar/Dropbox/Chapter_3/Biogeo_Hinter")
dir()

library(ape)
library(maps)
library(phytools)
library(phangorn)

MaxCred <- read.nexus("chrono_nr90hv5_2018.tre")

plot(MaxCred, cex=0.5, show.node.label=T)
axisPhylo()

#next command remove the outgroups
MaxCredNO <- drop.tip(MaxCred, c("Oritrophium_peruvianum", "Llerasia_caucana", "Soliva_sessilis", "Helianthus_annuus"))
plot(MaxCredNO, cex=0.5, show.node.label=T)
axisPhylo()

MaxCredNOLad <- ladderize(MaxCredNO)
plot(MaxCredNOLad, cex=0.5, show.node.label=T)

par(mfcol=c(1,2))
plot(MaxCred, cex=0.5)
axisPhylo()
plot(MaxCredNOLad, cex=0.5)
axisPhylo()

#check if the tree is ultrametic and save a new version 
is.ultrametric(MaxCredNOLad)
is.binary.tree(MaxCredNOLad)
MaxCredNOLadUL <- nnls.tree(cophenetic(MaxCredNOLad),MaxCredNOLad,rooted=T,trace=0)

plot(MaxCredNOLadUL, cex=0.5, show.node.label=T)
axisPhylo()

is.ultrametric(MaxCredNOLadUL)

write.tree(MaxCredNOLadUL, file="chrono_nr90hv5_2018_no.tree")