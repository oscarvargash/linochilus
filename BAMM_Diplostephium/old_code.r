








#Old code not needed for making transparencies bue perhaps useful

#######color transparency code
t_col <- function(color, percent = 50, name = NULL) {
  #	  color = color name
  #	percent = % transparency
  #	   name = an optional name for the color
  ## Get RGB values for named color
  rgb.val <- col2rgb(color)
  ## Make new color using input color as base and alpha set by transparency
  t.col <- rgb(rgb.val[1], rgb.val[2], rgb.val[3],
               max = 255,
               alpha = (100-percent)*255/100,
               names = name)
  ## Save the color
  invisible(t.col)
  
}
## END #######color transparency code

tr.lightblue <- t_col('lightblue', perc=40, name="lt.lightblue")
tr.gray80 <- t_col('gray80', perc=40, name="lt.gray80")

plot(1:10, col = tr.lightblue,
     pch = 16, cex = 5)
points((1:10) + 0.4, col=tr.gray80,
       pch = 16, cex = 4)

plot(1:10, col = rgb(red = 1, green = 0, blue = 0, alpha = 0.5),
     pch = 16, cex = 4)
points((1:10) + 0.4, col = rgb(red = 0, green = 0, blue = 1, alpha = 0.5),
       pch = 16, cex = 4)

###### old Diplostephium code for reference
##################################
#calculating rates for specific clades:
allrates <- getCladeRates(ed)
mean(allrates$lambda)
quantile(allrates$lambda, c(0.05, 0.95))

#mean(allrates$mu)
#quantile(allrates$mu, c(0.05, 0.95))

grias <- getCladeRates(ed, node=218)
mean(grias$lambda)

gustavia <- getCladeRates(ed, node=210)
mean(gustavia$lambda)

couropita <- getCladeRates(ed, node=207)
mean(couropita$lambda)

allantoma <- getCladeRates(ed, node=205)
mean(allantoma$lambda)

carinaria <- getCladeRates(ed, node=201)
mean(carinaria$lambda)

echinata <- getCladeRates(ed, node=203)
mean(echinata$lambda)

guianensis <- getCladeRates(ed, node=196)
mean(guianensis$lambda)

bertholletia <- getCladeRates(ed, node=115)
mean(bertholletia$lambda)

chartaceae <- getCladeRates(ed, node=187)
mean(chartaceae$lambda)

integrifolia <- getCladeRates(ed, node=172)
mean(integrifolia$lambda)

tetrapetala <- getCladeRates(ed, node=168)
mean(tetrapetala$lambda)

ollaria <- getCladeRates(ed, node=166)
mean(ollaria$lambda)

poiteau <- getCladeRates(ed, node=162)
mean(poiteau$lambda)

pisonis <- getCladeRates(ed, node=158)
mean(pisonis$lambda)

corytophora <- getCladeRates(ed, node=155)
mean(corytophora$lambda)

corrugata <- getCladeRates(ed, node=151)
mean(corrugata$lambda)

parvifolia <- getCladeRates(ed, node=122)
mean(parvifolia$lambda)


##################################
#If computeBayesFactors gives a matrix mm, and mm[2,1] is 10.0, this implies Bayes factor evidence
#of 10 in favor of the 2nd row model (a model with 1 process; e.g., rownames(mm)[2] over the first column model (a model with a single process).
bayes_factors <- computeBayesFactors(diplomcmcpb, expectedNumberOfShifts = 1, burnin=0.1)
capture.output (bayes_factors, file="bayes_factors.txt")
plotPrior(diplomcmcpb, xpectedNumberOfShifts = 1, burnin=0)
###############################
#crete a prior object CORE probabilities are greater than expected under the prior
#bpriors <- getBranchShiftPriors(diplotree, expectedNumberOfShifts = 1)
#summary(bpriors)
# compute the credible shift set
css <- credibleShiftSet(ed, expectedNumberOfShifts=1, threshold=4)
css$number.distinct
summary(css)
plot.credibleshiftset(css)
plot(css)
best <- getBestShiftConfiguration(ed, expectedNumberOfShifts=1, threshold = 3)
plot.bammdata(best, lwd=2, tau=0.002, legend=T)
addBAMMshifts(best)
#make coohort matrix
cmat <- getCohortMatrix (ed)
cohorts(cmat, ed, use.plot.bammdata=T, breaksmethod='jenks', lw=2)

#graph event data with best shift configuration

plot.bammdata(ed, lwd=2, tau=0.002, legend=T, breaksmethod='linear')
addBAMMshifts(ed)

plot.bammdata(ed, lwd=2, tau=0.002, legend=T, color.interval=c(NA,2), breaksmethod='linear')
addBAMMshifts(ed)

plot.bammdata(ed, lwd=2, tau=0.002, legend=T, breaksmethod='jenks')
addBAMMshifts(ed)

####plotting rates trhough time


st <- max(branching.times(lecytree))
plotRateThroughTime(ed, intervalCol="black", avgCol="black", start.time=st, ylim=c(0,2), cex.axis=2)
text(x=10, y=1.5, label="All sampling", font=4, cex=2.0, pos=4)
