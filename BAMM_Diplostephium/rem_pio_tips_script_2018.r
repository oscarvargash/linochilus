#install.packages("phytools")
#install.packages("maps")
#install.packages("ape")
#install.packages()

setwd("/Users/oscar/Dropbox/Chapter_3/BAMM/Diplo_2018")
dir()

library(ape)
library(maps)
library(phytools)
library(phangorn)

MaxCredNOLadUL <- read.tree("chrono_nr90hv5_2018_no.tree")

plot(MaxCredNOLadUL, cex=0.5, show.node.label=T)
axisPhylo()

#print all taxa
sort(MaxCredNOLadUL$tip.label)

#next command remove the outgroups
Diplo_tree <- drop.tip(MaxCredNOLadUL, c("Exostigma_notobellidiastrum","Archibaccharis_asperifolia","Aztecaster_matudae","Baccharis_genistelloides","Baccharis_tricuneata","Blakiella_bartsiifolia","Floscaldasia_hypsophila","Heterothalamus_alienus","Hinterhubera_ericoides","Laennecia_sophiifolia","Laestadia_muscicola","Lagenophora_cuchumatanica","Piofontia_alveolata","Piofontia_antioquensis","Piofontia_apiculata","Piofontia_camargoana","Piofontia_cayambensis","Piofontia_cinerascens","Piofontia_colombiana","Piofontia_coriacea","Piofontia_costaricensis","Piofontia_eriophora","Piofontia_floribunda","Piofontia_frontinensis","Piofontia_glutinosa","Piofontia_heterophylla","Piofontia_huertasii","Piofontia_inesiana","Piofontia_jaramilloi","Piofontia_jenesana","Piofontia_juajibioyi","Piofontia_lacunosa","Piofontia_mutiscuana","Piofontia_oblongifolia","Piofontia_obtusa","Piofontia_ochracea","Piofontia_phylicoidea","Piofontia_revoluta","Piofontia_rhododendroides","Piofontia_rhomboidalis_COL","Piofontia_rhomboidalis_ECU","Piofontia_romeroi","Piofontia_rosmarinifolia","Piofontia_rupestris","Piofontia_schultzii_CAL","Piofontia_schultzii_CUN","Piofontia_sp_nov_ANT","Piofontia_tachirensis","Piofontia_tenuifolia","Piofontia_venezuelensis","Piofontia_violacea","Westoniella_kohkemperi"))

plot(Diplo_tree, cex=0.5, show.node.label=T)
axisPhylo()

is.ultrametric(Diplo_tree)

write.tree(Diplo_tree, file="diplo_chrono_nr90hv5_2018_no.tree")

Diplo_tree <- read.tree("diplo_chrono_nr90hv5_2018_no.tree")
sort(Diplo_tree$tip.label)