install.packages("BAMMtools")
install.packages("strap")

library(phytools)
library(BAMMtools)
library(phangorn)
library(strap)
#update.packages("BAMMtools")
setwd("/Users/Oscar/Dropbox/Chapter_3/BAMM/Diplo_2018")
dir()

v <- read.tree("diplo_chrono_nr90hv5_2018_no.tree")
plot.phylo(v, cex=0.4)
axisPhylo()
is.ultrametric(v)
is.binary.tree(v)
min(v$edge.lenght)

#estimate the set of priors for bamm based on the tree
setBAMMpriors(v)

#read results from BAMM
diplomcmc <- read.csv("mcmc_out.txt", header=T)
plot(diplomcmc$logLik)
plot(diplomcmc$logLik ~ diplomcmc$generation)

diplomcmcpb <- diplomcmc[1000:10000, ]
plot(diplomcmcpb$logLik)
post_probs <- table(diplomcmcpb$N_shifts) / nrow(diplomcmcpb)
post_probs
capture.output(post_probs, file="post_probs.txt")

##estimate effective sample size for log likelihoods AND numbers of shfts
library(coda)
effectiveSize(diplomcmcpb$N_shifts)
effectiveSize(diplomcmcpb$logLik)
##effective sample size should be more than 100, 500 better

##visualize
diplotree <- read.tree("diplo_chrono_nr90hv5_2018_no.tree")
plot.phylo(diplotree, cex=0.5)
nodelabels(cex=0.5)
ed <- getEventData(diplotree, "event_data.txt", burnin=0.25, nsamples=9000)
summary(ed)
plot.bammdata(ed, lwd=2, tau=0.002, legend=T, breaksmethod='linear')

##plots rate through time
st <- max(branching.times(diplotree))

plotRateThroughTime(ed, avgCol="black", start.time=st, ylim=c(0,2), cex.axis=2, 
                    ratetype =  'extinction', intervalCol='gray80', intervals=c(0.05, 0.95), opacity=0.6)
plotRateThroughTime(ed, avgCol="blue", start.time=st, ylim=c(0,2), cex.axis=2, 
                    add=T, intervalCol="lightblue", intervals=c(0.05, 0.95), opacity=0.6)
legend('topleft', legend=c('Speciation','Extinction'), col=c('blue','black'),
       fill=c("lightblue",'gray80'), border=FALSE, lty=1, lwd=2, merge=TRUE,
       seg.len=0.6)
text(x=5.5, y=2, label="Diplostephium", font=4, cex=2.0, pos=4)


#calculating average rates for the whole tree:
allrates <- getCladeRates(ed)
mean(allrates$lambda)
quantile(allrates$lambda, c(0.05, 0.95))

mean(allrates$mu)
quantile(allrates$mu, c(0.05, 0.95))

#finally calculate speciation rates using Magallón and Sanderson's formula
library(geiger)
bd.ms(time=6.46, n=63, phy=NULL, epsilon = 0, missing = 0, crown=TRUE)




